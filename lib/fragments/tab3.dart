import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:untitled/constrants/str_constants.dart';
import '../constrants/custom_colors.dart';
import '../widgets/step_item1.dart';
import '../widgets/step_item3.dart';
import '../widgets/step_item_with_background.dart';

class Tab3 extends StatelessWidget {
  const Tab3({super.key});

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return ResponsiveBuilder(
      builder: (context, sizingInformation) {
        if (sizingInformation.deviceScreenType == DeviceScreenType.desktop) {
          var arrow_1_2Height = 0.0;
          var arrow_1_2Width = 0.0;
          var arrow_2_3Height = 0.0;
          var arrow_2_3Width = 0.0;
          var arrow_1_2TopMargin = 0.0;
          var arrow_1_2LeftMargin = 0.0;
          var arrow_2_3TopMargin = 0.0;
          var arrow_2_3LeftMargin = 0.0;
          if(size.width > 950 && size.width < 1000)
          {
            arrow_1_2Height = size.height * 0.34;
            arrow_1_2Width = size.width * 0.59;
            arrow_2_3Height = size.height * 0.28;
            arrow_2_3Width = size.width * 0.55;



            arrow_1_2TopMargin = size.height*0.37;
            arrow_1_2LeftMargin = 0.0;
            arrow_2_3TopMargin = size.height*0.9;
            arrow_2_3LeftMargin = size.width*0.06;

          }else if(size.width > 1001 && size.width < 1200)
          {
            arrow_1_2Height = size.height * 0.34;
            arrow_1_2Width = size.width * 0.67;
            arrow_2_3Height = size.height * 0.28;
            arrow_2_3Width = size.width * 0.55;

            arrow_1_2TopMargin = size.height*0.36;
            arrow_1_2LeftMargin = 0.0;
            arrow_2_3TopMargin = size.height*0.9;
            arrow_2_3LeftMargin = size.width*0.06;

          }else if(size.width > 1201 && size.width < 1350)
          {
            arrow_1_2Height = size.height * 0.33;
            arrow_1_2Width = size.width * 0.7;
            arrow_2_3Height = size.height * 0.28;
            arrow_2_3Width = size.width * 0.55;

            arrow_1_2TopMargin = size.height*0.35;
            arrow_1_2LeftMargin = 0.0;
            arrow_2_3TopMargin = size.height*0.9;
            arrow_2_3LeftMargin = size.width*0.06;

          }
          else if(size.width > 1351 && size.width < 1450)
          {
            arrow_1_2Height = size.height * 0.32;
            arrow_1_2Width = size.width * 0.62;
            arrow_2_3Height = size.height * 0.28;
            arrow_2_3Width = size.width * 0.55;

            arrow_1_2TopMargin = size.height*0.36;
            arrow_1_2LeftMargin = size.width*0.06;
            arrow_2_3TopMargin = size.height*0.88;
            arrow_2_3LeftMargin = size.width*0.13;

          }else if(size.width < 2000){
            arrow_1_2Height = size.height * 0.34;
            arrow_1_2Width = size.width * 0.59;
            arrow_2_3Height = size.height * 0.28;
            arrow_2_3Width = size.width * 0.55;

            arrow_1_2TopMargin = size.height*0.3;
            arrow_1_2LeftMargin = size.width*0.15;
            arrow_2_3TopMargin = size.height*0.8;
            arrow_2_3LeftMargin = size.width*0.18;
          }

          return ListView(
            padding: EdgeInsets.zero,
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: [
              const Center(
                child: Text(
                  StrConstants.drei,
                  style: TextStyle(
                    fontSize: 40,
                    color: CustomColors.black,
                  ),
                ),
              ),
              Stack(
                children: [
                  StepItem1(
                    bgCircleDesign: Container(
                      height: size.height * 0.25,
                      width: size.height * 0.25,
                      decoration: const BoxDecoration(
                          shape: BoxShape.circle, color: CustomColors.grey100),
                    ),
                    stepNumber: "1.",
                    description: StrConstants.erstellen,
                    imagePath: 'assets/images/step1.png',
                  ),
                  Padding(
                    padding: EdgeInsets.only(top: size.height * 0.54),
                    child: const StepItemWithBackground(
                      stepNumber: "2.",
                      description: StrConstants.erstellenStep2Tab3,
                      backgroundPath: 'assets/images/step_item_2_bg.png',
                      foregroundPath: 'assets/images/step2_tab3.png',
                    ),
                  ),
                  const SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.only(top: size.height),
                    child: StepItem3(
                      bgCircleDesign: Container(
                        height: size.height * 0.35,
                        width: size.height * 0.35,
                        decoration: const BoxDecoration(
                            shape: BoxShape.circle, color: CustomColors.grey100),
                      ),
                      stepNumber: "3.",
                      description: StrConstants.vermittlung,
                      imagePath: 'assets/images/step3_tab3.png',
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: arrow_1_2TopMargin, left: arrow_1_2LeftMargin),
                    child: Image.asset(
                      "assets/images/arrow_1_2.png",
                      height: arrow_1_2Height,
                      width: arrow_1_2Width,
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                        top: arrow_2_3TopMargin, left: arrow_2_3LeftMargin),
                    child: Image.asset(
                      "assets/images/arrow_2_3.png",
                      height: arrow_2_3Height,
                      width: arrow_2_3Width,
                    ),
                  ),
                ],
              ),
            ],
          );
        } else {
          return ListView(
            shrinkWrap: true,
            physics: const NeverScrollableScrollPhysics(),
            children: [
              const Center(
                child: Text(
                  StrConstants.drei,
                  style: TextStyle(
                    fontSize: 21,
                    color: CustomColors.black,
                  ),
                ),
              ),
              StepItem1(
                bgCircleDesign: Container(),
                stepNumber: "1.",
                description: StrConstants.erstellen,
                imagePath: 'assets/images/step1.png',
              ),
              const StepItemWithBackground(
                stepNumber: "2.",
                description: StrConstants.erstellen,
                backgroundPath: 'assets/images/step_item_2_bg_mobile.png',
                foregroundPath: 'assets/images/step2.png',
              ),
              const SizedBox(height: 20),
              StepItem3(
                bgCircleDesign: Container(),
                stepNumber: "3.",
                description: StrConstants.mit,
                imagePath: 'assets/images/step3.png',
              ),
            ],
          );
        }
      },
    );
  }
}
