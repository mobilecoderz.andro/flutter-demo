import 'package:flutter/material.dart';
import 'package:untitled/constrants/custom_colors.dart';


class GradientElevatedButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;
  final List<Color> gradientColors;
  final double borderRadius;
  final double buttonHeight;
  final double buttonFontSize;

  const GradientElevatedButton({
    Key? key,
    required this.text,
    required this.onPressed,
    required this.gradientColors,
    this.borderRadius = 12.0,
    this.buttonHeight = 40.0,
    this.buttonFontSize = 14.0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Stack(
      children: [
        Container(
          height: buttonHeight,
          width: size.width,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: gradientColors,
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
            borderRadius: BorderRadius.circular(borderRadius),
          ),
          child: ElevatedButton(
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.transparent,
              foregroundColor: CustomColors.white,
              shadowColor: Colors.transparent,
              textStyle: TextStyle(
                fontSize: buttonFontSize,
              ),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(borderRadius),
              ),
            ),
            onPressed: onPressed,
            child: Text(text),
          ),
        ),
      ],
    );
  }
}