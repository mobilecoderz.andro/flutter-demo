import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:untitled/constrants/str_constants.dart';
import 'package:untitled/widgets/gradient_elevated_button.dart';

class DesktopBodyWidget extends StatefulWidget {
  const DesktopBodyWidget({super.key});

  @override
  State<DesktopBodyWidget> createState() => _DesktopBodyWidgetState();
}

class _DesktopBodyWidgetState extends State<DesktopBodyWidget> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return ResponsiveBuilder(
        builder: (context, sizingInformation) {
          var buttonWidth = 0.0;
          var titleFontSize = 0.0;
          var buttonFontSize = 0.0;
          var buttonHeight = 0.0;
          if (sizingInformation.deviceScreenType == DeviceScreenType.tablet) {
            titleFontSize = 30;
            buttonWidth = 200;
            buttonFontSize = 12;
            buttonHeight = 30;
          } else
          if (sizingInformation.deviceScreenType == DeviceScreenType.desktop) {
            titleFontSize = 60;
            buttonWidth = 300;
            buttonFontSize = 14;
            buttonHeight = 40;
          }


          return Container(
            height: size.height * 0.66,
            width: size.width,
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/header_bg.png"),
                fit: BoxFit.cover,
                alignment: Alignment.topCenter,
              ),
            ),
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: size.width * 0.07),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Deine Job",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: titleFontSize, height: 1),
                      ),
                      SizedBox(
                        height: size.height * 0.01,
                      ),
                      Text(
                        "website",
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: titleFontSize, height: 1),
                      ),
                      SizedBox(
                        height: size.height * 0.04,
                      ),
                      Container(
                        width: buttonWidth,
                        padding: EdgeInsets.symmetric(vertical: 4),
                        child: GradientElevatedButton(
                          buttonHeight: buttonHeight,
                          buttonFontSize: buttonFontSize,
                          text: StrConstants.kostenlos,
                          onPressed: () {},
                          gradientColors: const [
                            Color.fromARGB(255, 49, 151, 149),
                            Color.fromARGB(255, 49, 130, 206),
                          ],
                          borderRadius: 12.0,
                        ),
                      ),

                    ],
                  ),
                  SizedBox(
                    width: size.width * 0.05,
                  ),
                  Container(
                    height: size.height * 0.45,
                    width: size.height * 0.45,
                    decoration: const BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                        image: DecorationImage(image: AssetImage("assets/images/hand.png"))),
                  )
                ],
              ),
            ),
          );
        });
  }
}
