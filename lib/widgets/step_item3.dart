import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:untitled/constrants/custom_colors.dart';


class StepItem3 extends StatefulWidget {
  final String stepNumber;
  final String description;
  final String imagePath;
  final Widget bgCircleDesign;


  const StepItem3({
    required this.stepNumber,
    required this.description,
    required this.imagePath,
    required this.bgCircleDesign,
  });

  @override
  State<StepItem3> createState() => _StepItemState();
}

class _StepItemState extends State<StepItem3> {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
        mobile: (BuildContext context) => Padding(
          padding:const EdgeInsets.only(left: 40.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    widget.stepNumber,
                    style: const TextStyle(
                      fontSize: 130,
                      color: CustomColors.grey,
                    ),
                  ),
                  const SizedBox(width: 20),
                  Container(
                    margin: const EdgeInsets.only(top:10.0),
                    child: Text(
                      widget.description,
                      style: const TextStyle(
                        fontSize: 16,
                        color: CustomColors.grey,
                      ),
                    ),
                  ),
                ],
              ),

              Transform.translate(
                offset: const Offset(0, -50), // Move the image upwards by 30 pixels
                child: Image.asset(
                  widget.imagePath,
                  width: 281,
                  height: 210,
                ),
              ),

            ],
          ),
        ),
        tablet: (BuildContext context) => Padding(
          padding: const EdgeInsets.symmetric(vertical: 30.0, horizontal: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                widget.stepNumber,
                style: const TextStyle(
                  fontSize: 130,
                  color: CustomColors.grey,
                ),
              ),
              Column(
                children: [
                  Image.asset(widget.imagePath,height: 180,),
                  Container(
                    margin: const EdgeInsets.only(top:10.0),
                    child: Text(
                      widget.description,
                      style: const TextStyle(
                        fontSize: 16,
                        color: CustomColors.grey,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        desktop: (BuildContext context) => Padding(
          padding: const EdgeInsets.symmetric(vertical: 50.0, horizontal: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Stack(
                children: [
                  widget.bgCircleDesign,
                  Transform.translate(
                    offset: const Offset(0, -20),
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 80.0,),
                          child: Text(
                            widget.stepNumber,
                            style: const TextStyle(
                              fontSize: 130,
                              color: CustomColors.grey,
                            ),
                          ),
                        ),
                        const SizedBox(width: 10),
                        Container(
                          margin: const EdgeInsets.only(top:10.0),
                          child: Text(
                            widget.description,
                            style: const TextStyle(
                              fontSize: 30,
                              color: CustomColors.grey,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),

              const SizedBox(width: 20),
              Image.asset(widget.imagePath,height: 376,width: 502,),
            ],
          ),
        )
      );

  }
}

