import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:untitled/constrants/custom_colors.dart';


class StepItem1 extends StatefulWidget {
  final String stepNumber;
  final String description;
  final String imagePath;
  final Widget bgCircleDesign;


  const StepItem1({
    required this.stepNumber,
    required this.description,
    required this.imagePath,
    required this.bgCircleDesign,
  });

  @override
  State<StepItem1> createState() => _StepItemState();
}

class _StepItemState extends State<StepItem1> {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
        mobile: (BuildContext context) => Padding(
          padding: const EdgeInsets.symmetric(vertical: 30.0, horizontal: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.only(top: 90),
                child: Text(
                  widget.stepNumber,
                  style: const TextStyle(
                    fontSize: 130,
                    color: CustomColors.grey,
                  ),
                ),
              ),
              Column(
                children: [
                  Image.asset(widget.imagePath,height: 145,width: 220,),
                  SizedBox(height: 25,),
                  Container(
                    margin: const EdgeInsets.only(top:10.0),
                    child: Text(
                      widget.description,
                      style: const TextStyle(
                        fontSize: 16,
                        color: CustomColors.grey,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        tablet: (BuildContext context) => Padding(
          padding: const EdgeInsets.symmetric(vertical: 30.0, horizontal: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                widget.stepNumber,
                style: const TextStyle(
                  fontSize: 130,
                  color: CustomColors.grey,
                ),
              ),
              Column(
                children: [
                  Image.asset(widget.imagePath,height: 180,),
                  Container(
                    margin: const EdgeInsets.only(top:10.0),
                    child: Text(
                      widget.description,
                      style: const TextStyle(
                        fontSize: 16,
                        color: CustomColors.grey,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        desktop: (BuildContext context) => Padding(
          padding: const EdgeInsets.symmetric(vertical: 50.0, horizontal: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Stack(
                children: [
                  widget.bgCircleDesign,
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    child: Text(
                      widget.stepNumber,
                      style: const TextStyle(
                        fontSize: 130,
                        color: CustomColors.grey,
                      ),
                    ),
                  ),
                ],
              ),
              const SizedBox(width: 20),
              Container(
                margin: const EdgeInsets.only(top:70.0),
                child: Text(
                  widget.description,
                  style: const TextStyle(
                    fontSize: 30,
                    color: CustomColors.grey,
                  ),
                ),
              ),
              const SizedBox(width: 20),
              Image.asset(widget.imagePath,height: 253,width: 384,),
            ],
          ),
        )
      );

  }
}

