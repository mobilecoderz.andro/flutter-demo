import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:untitled/constrants/custom_colors.dart';
import 'package:untitled/constrants/str_constants.dart';
import 'package:untitled/widgets/gradient_elevated_button.dart';

class MobileBodyWidget extends StatefulWidget {
  const MobileBodyWidget({super.key});

  @override
  State<MobileBodyWidget> createState() => _MobileBodyWidgetState();
}

class _MobileBodyWidgetState extends State<MobileBodyWidget> {
  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
          return SingleChildScrollView(
            child: Stack(
              children: [
                Container(
                  height: size.height * 0.9,
                  width: size.width,
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.centerRight,
                      end: Alignment.centerLeft,
                      colors: <Color>[
                        Color.fromRGBO(235, 244, 255, 1),
                        Color.fromRGBO(230, 255, 250, 1)
                      ],
                    ),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: Text(
                          StrConstants.deine,
                          textAlign: TextAlign.center,
                          style: TextStyle(fontSize: size.height * 0.06, height: 1),
                        ),
                      ),
                      Container(
                        width: size.width,
                        child: Image.asset(
                          "assets/images/hand.png",
                          fit: BoxFit.cover,
                        ),
                      )
                    ],
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: size.height * 0.8),
                  child: Container(
                    height: 140,
                    width: size.width,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border(
                          top: BorderSide(width: 5.0, color: Colors.grey.shade300),
                        ),
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(12),
                            topRight: Radius.circular(12))),
                    child: Column(
                      children: [
                        SizedBox(
                          height: size.height * 0.03,
                        ),
                        Center(
                          child: Container(
                            margin: EdgeInsets.symmetric(horizontal: 20),
                            padding: EdgeInsets.symmetric(vertical: 4),
                            width: size.width,
                            child: GradientElevatedButton(
                              buttonFontSize: 14,
                              buttonHeight: 40,
                              text: StrConstants.kostenlos,
                              onPressed: () {},
                              gradientColors: const [
                                Color.fromARGB(255, 49, 151, 149),
                                Color.fromARGB(255, 49, 130, 206),
                              ],
                              borderRadius: 12.0,
                            ),
                          ),
                        ),

                      ],
                    ),
                  ),
                )
              ],
            ),
          );
  }
}


