import 'package:flutter/material.dart';

class CustomColors {
  static const Color grey = Color(0xff718096);
  static const Color grey200 = Color(0xffCBD5E0);
  static const Color white = Color(0xffffffff);
  static const Color black = Color(0xff4A5568);
  static const Color blue = Color(0xff1E1C44);
  static const Color seyan = Color(0xff319795);
  static const Color seyanLight = Color(0xff81E6D9);
  static const Color grey100 = Color.fromRGBO(187, 187, 189, 0.1);


}
