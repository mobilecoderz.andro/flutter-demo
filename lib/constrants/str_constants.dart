


class StrConstants {
  // App Name
  static const appName = 'Text Task';
  static const login = 'Login';

  static const flutterTestIndex = 'Flutter\nTest\nIndex';
  static const yeh_you = 'yeh you rocks';
  static const now_you = ', now you are in next level ';


  static const now_you_can = 'Now you can create the Test page\nand show skilled you are';
  static const only_front = '-Only Frontend';
  static const with_flutter = '-With Flutter';
  static const full_resp = '-Full Responsive Webview';
  static const create_this = 'Create this Test page and push to the GitLab';
  static const send_me = 'Send me vie Slack a message when you start the and when you will be finished';
  static const the_website = 'The website can be scrolled the buttons, "Koestonlos" goes on the top bar';
  static const mobile_view = 'Mobile View';
  static const desktop_view = 'DeskTop View';
  static const desktop_scroll = 'DeskTop Scroll';
  static const kostenlos = 'Kostenlos Registrieren';
  static const deine = 'Deine Job\nwebsite';
  static const arbeitnehmer = 'Arbeitnehmer';
  static const arbeitgeber = 'Arbeitgeber';
  static const temporrbro = 'Temporärbüro';
  static const drei = 'Drei einfache Schritte\nzu deinem neuen Job';
  static const erstellen = 'Erstellen dein Lebenslauf';
  static const erstellenTab2 = 'Erstellen dein Unternehmensprofil';
  static const erstellenStep2Tab2 = 'Erstellen ein Jobinserat';
  static const erstellenStep2Tab3 = 'Erhalte Vermittlungs-\nangebot von Arbeitgeber';
  static const mit = 'Mit nur einem Klick\nbewerben';
  static const vermittlung = 'Vermittlung nach\nProvision oder\nStundenlohn';


}