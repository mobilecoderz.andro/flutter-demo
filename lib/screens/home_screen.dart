import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';
import 'package:untitled/constrants/custom_colors.dart';
import 'package:untitled/constrants/str_constants.dart';
import 'package:untitled/fragments/tab1.dart';
import 'package:untitled/widgets/custom_app_bar.dart';
import 'package:untitled/widgets/mobile_body_widget.dart';

import '../fragments/tab2.dart';
import '../fragments/tab3.dart';
import '../widgets/desktop_body_widget.dart';


class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with SingleTickerProviderStateMixin {
  late TabController _tabController;
  BorderRadius? _indicatorBorderRadius;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: const PreferredSize(
            preferredSize: const Size.fromHeight(65.0), child: CustomAppBar()),
      body: ResponsiveBuilder(
    builder: (context, sizingInformation) {
      var size = MediaQuery.of(context).size;
      var tabPadding = 0.0;
    if (sizingInformation.deviceScreenType == DeviceScreenType.mobile) {
      tabPadding = size.width * 0.1;
    }else if (sizingInformation.deviceScreenType == DeviceScreenType.desktop) {

    }

      return ListView(
        shrinkWrap: true,
        children: [
          ScreenTypeLayout.builder(
            mobile: (BuildContext context) => const MobileBodyWidget(),
            tablet: (BuildContext context) => const DesktopBodyWidget(),
            desktop: (BuildContext context) => const DesktopBodyWidget(),
          ),
          const SizedBox(height: 40,),
          SizedBox(
            child: TabBar(
              tabAlignment: TabAlignment.center,
              controller: _tabController,
              tabs:  [
                _buildTab(StrConstants.arbeitnehmer, BorderRadius.only(topLeft: Radius.circular(20), bottomLeft: Radius.circular(20)),Border(left: BorderSide(color: CustomColors.grey200, width: 1),top: BorderSide(color: CustomColors.grey200, width: 1),bottom: BorderSide(color: CustomColors.grey200, width: 1))),
                _buildTab(StrConstants.arbeitgeber,  BorderRadius.zero,Border.all(color: CustomColors.grey200, width: 1)),
                _buildTab(StrConstants.temporrbro,  BorderRadius.only(topRight: Radius.circular(20), bottomRight: Radius.circular(20)), Border(right: BorderSide(color: CustomColors.grey200, width: 1),top: BorderSide(color: CustomColors.grey200, width: 1),bottom: BorderSide(color: CustomColors.grey200, width: 1))),
              ],
              labelPadding: EdgeInsets.zero,
              isScrollable: true,
              indicator: ShapeDecoration(
                shape: RoundedRectangleBorder(
                  borderRadius: _indicatorBorderRadius ?? const BorderRadius.only(
                    topLeft: Radius.circular(20),
                    bottomLeft: Radius.circular(20),
                  ),
                ),
                color: CustomColors.seyanLight,
              ),

              indicatorSize: TabBarIndicatorSize.tab,
              labelColor: CustomColors.white,
              unselectedLabelColor: CustomColors.seyan,
              dividerHeight: 0,
              indicatorWeight: 0,
              onTap: (index) {
                setState(() {
                  if (index == 0) {
                    _indicatorBorderRadius = const BorderRadius.only(
                      topLeft: Radius.circular(20),
                      bottomLeft: Radius.circular(20),
                    );
                  } else if (index == _tabController.length - 1) {
                    _indicatorBorderRadius = const BorderRadius.only(
                      topRight: Radius.circular(20),
                      bottomRight: Radius.circular(20),
                    );
                  } else {
                    _indicatorBorderRadius = BorderRadius.circular(0);
                  }
                });
              },
            ),
          ),
          const SizedBox(height: 20,),
          SizedBox(
            // height: MediaQuery.of(context).size.height + (MediaQuery.of(context).size.height * 0.5), // Remaining height for TabBarView
            height: MediaQuery.of(context).size.height * 2, // Remaining height for TabBarView
            child: TabBarView(
              physics: const NeverScrollableScrollPhysics(),
              controller: _tabController,
              children: const [
                Center(child: Tab1()),
                Center(child: Tab2()),
                Center(child: Tab3()),
              ],
            ),
          ),
        ],
      );
    }),
    );
  }


  Widget _buildTab(String text, BorderRadius borderRadius, Border borderSide) {
    return Tab(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: borderRadius,
          border: borderSide,
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 40),
          child: Align(
            alignment: Alignment.center,
            child: Tab(text: text),
          ),
        ),
      ),
    );
  }

}

